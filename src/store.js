import { createStore, compose } from "redux";
import rootReducer from "./reducers";
import initialState from "./data.js";

const store = createStore(
  rootReducer,
  initialState,
  compose(
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

store.subscribe(() => {
  console.log("store changed", store.getState());
});
export default store;
