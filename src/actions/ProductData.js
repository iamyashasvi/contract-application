import { ADD_UUID, ADD_TO_CART, CHANGE_ITEMS } from "./types";
import { v4 as uuidv4 } from "uuid";
import initialState from "../data";
let product = initialState.product;
export const addUUID = (title) => {
  let uuid = uuidv4();
  return {
    type: ADD_UUID,
    uuid: uuid,
    title: title,
  };
};

export const addToCart = (title) => {
  console.log("action");
  return {
    type: ADD_TO_CART,
    title,
    product,
  };
};

export const itemChange = (title, e) => {
  return {
    type: CHANGE_ITEMS,
    text: e,
    title,
    product,
  };
};



