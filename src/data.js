const initialState = {
    product: [
      {
        title: "Desktop publishing software",
        subtitle: "Many desktop publishing package and web page editor now.",
        tax: 0,
        price: 180.0,
        oldPrice: 230.0,
        stock:30,
        category:"Software",
        "Product description": [
          "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."," The point of using Lorem lpsum is.",
          "There are many variations of passages of Lorem lpsum available, but the majority have suffered alteration in some form, by injected humore, or randomised words which don't look even slightly believable.",
        ],
        description: [
          {
            title: "Description lists",
            content: ["A description list is perfect for definig terms."],
          },
        ],
        image: [
          "https://via.placeholder.com/800x900.png?text=DESKTOP-PUBLISHING1",
          "https://via.placeholder.com/800x900.png?text=DESKTOP-PUBLISHING2",
          "https://via.placeholder.com/800x900.png?text=DESKTOP-PUBLISHING3",
        ],
      },
      {
        title: "Text editor",
        subtitle: "Many desktop publishing package and web page editor now.",
        tax: 0,
        price: 50.0,
        oldPrice: 63.0,
        stock:80,
        category:"Application",
        "Product description": [
          "There are many variations of passages of Lorem lpsum available",
          "The majority have suffered alteration in some form, by injected humore, or randomised words which don't look even slightly believable.",
        ],
        description: [
          {
            title: "Description lists",
            content: ["List is perfect for defining terms."],
          },
        ],
        image: [
          "https://via.placeholder.com/800x900.png?text=TEXT-EDITOR1",
          "https://via.placeholder.com/800x900.png?text=TEXT-EDITOR2",
          "https://via.placeholder.com/800x900.png?text=TEXT-EDITOR3",
        ],
      },
      {
        title: "CRM software",
        subtitle: "Many desktop publishing package and web page editor now.",
        tax: 0,
        price: 110,
        oldPrice: 0,
        stock:60,
        category:"Software",
        "Product description": [
          "Distracted the readable content of a page when looking at its layout. The point of using Lorem lpsum is.",
          "There are many variations of passages of Lorem lpsum available, but the majority have suffered alteration in some form, by injected humore, or randomised words which don't look even slightly believable.",
        ],
        description: [
          {
            title: "Description lists",
            content: ["A description list is perfect for defining terms."],
          },
        ],
        image: [
          "https://via.placeholder.com/800x900.png?text=CRM-SOFTWARE1",
          "https://via.placeholder.com/800x900.png?text=CRM-SOFTWARE2",
          "https://via.placeholder.com/800x900.png?text=CRM-SOFTWARE3",
        ],
      },
      {
        title: "Photo editor",
        subtitle: "Many desktop publishing package and web page editor now.",
        tax: 0,
        price: 700.0,
        oldPrice: 0,
        stock:90,
        category:"Application",
        "Product description": [
          "Page when looking at its layout. The point of using Lorem lpsum us.",
          "There are many variations of passages of Lorem lpsum available, but the majority have suffered alteration in some form, by injected humore, or randomised words which don't look even slightly believable.",
        ],
        description: [
          {
            title: "Description lists",
            content: ["A description list is perfect for definig terms."],
          }
        ],
        image: [
          "https://via.placeholder.com/800x900.png?text=PHOTO-EDITOR1",
          "https://via.placeholder.com/800x900.png?text=PHOTO-EDITOR2",
          "https://via.placeholder.com/800x900.png?text=PHOTO-EDITOR3",
        ],
      },
    ],
    cart:[],
  };

  export default initialState;
  