import React, { Component } from "react";

class LikeAndLove extends Component {
  constructor(props) {
    super(props);
    this.state = {
      like: false,
      love: false,
    };
  }

  toggle = (buttonType) => {
    return (e) => {
      this.setState({ [buttonType]: !this.state[buttonType] });
    };
  };
  render() {
    return (
      <div className="d-flex justify-content-start">
        <button
          type="button"
          className="btn btn-outline-secondary mt-2"
          onClick={this.toggle("like")}
        >
          {this.state.like ? (
            <span>
              <i className="fa fa-thumbs-up" aria-hidden="true"></i> Liked
            </span>
          ) : (
            <span>
              <i className="fa fa-thumbs-o-up" aria-hidden="true"></i> Like
            </span>
          )}
        </button>
        <button
          type="button"
          className="btn btn-outline-secondary ml-2 mt-2"
          onClick={this.toggle("love")}
        >
          {this.state.love ? (
            <span>
              <i className="fa fa-heart" aria-hidden="true"></i> Loved
            </span>
          ) : (
            <span>
              <i className="fa fa-heart-o" aria-hidden="true"></i> Love
            </span>
          )}
        </button>
      </div>
    );
  }
}

export default LikeAndLove;
