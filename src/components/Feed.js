import React from "react";
import Message from "./feed-components/Message";
import Photo from "./feed-components/Photo";
import Following from "./feed-components/Following";
import Love from "./feed-components/Love";

function Feed(props) {
  return props.tabs.map((post) => {
    switch (post.type) {
      case "message":
        return (
          <Message image={props.values.images} {...post} key={post.time} />
        );
      case "photo":
        return <Photo image={props.values.images} {...post} key={post.time} />;
      case "following":
        return (
          <Following image={props.values.images} {...post} key={post.time} />
        );
      case "love":
        return <Love image={props.values.images} {...post} key={post.time} />;
      default:
        return <p>Something went wrong</p>;
    }
  });
}
export default Feed;
