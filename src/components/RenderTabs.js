import React, { Component } from "react";
import { BrowserRouter, Route, Switch, NavLink } from "react-router-dom";
import Feed from "./Feed";

class RenderTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newData: this.props.values.userMessage,
      oldData: this.props.values.lastActivity,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.values !== prevProps.values) {
      this.setState({
        newData: this.props.values.userMessage,
        oldData: this.props.values.lastActivity,
      });
    }
  }

  render() {
    const classNames = {
      one: "nav-link " + this.props.values.one,
      two: "nav-link " + this.props.values.two,
    };
    return (
      <BrowserRouter>
        <Switch>
          <>
            <div className="container mt-5 mb-5">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <div
                    className={classNames.one}
                    onClick={this.props.activeState("one")}
                  >
                    <NavLink
                      to="/project"
                      isActive={(match) => {
                        if (!match) {
                          return false;
                        }
                      }}
                    >
                      User messages
                    </NavLink>
                  </div>
                </li>
                <li className="nav-item">
                  <div
                    className={classNames.two}
                    onClick={this.props.activeState("two")}
                  >
                    <NavLink
                      to="/project/last-activity"
                      isActive={(match) => {
                        if (!match) {
                          return false;
                        }
                      }}
                    >
                      Last Activity
                    </NavLink>
                  </div>
                </li>
              </ul>
              <Route
                exact
                path="/project"
                render={() => (
                  <Feed tabs={this.state.newData} {...this.props} />
                )}
              />
              <Route
                exact
                path="/project/last-activity"
                render={() => (
                  <Feed tabs={this.state.oldData} {...this.props} />
                )}
              />
            </div>
          </>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default RenderTabs;
