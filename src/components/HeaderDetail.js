import React, { Component } from "react";
import Progress from "./Progress";
import Images from "./Images";
import moment from "moment";
class HeaderDetail extends Component {
  render() {
    return (
      <div className="container">
        <div className="row mt-5">
          <div className="col-8 d-flex justify-content-start">
            <h1 className="font-weight-light">
              Contract with {this.props.values.client}
            </h1>
          </div>
          <div className="col-4 d-flex justify-content-end align-items-center">
            <button type="button" className="btn btn-outline-secondary btn-sm">
              Edit project
            </button>
          </div>
        </div>
        <div className="row mt-5 d-xs-flex flex-xs-column">
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Status:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <button type="button" className="btn btn-success p-1 mb-3">
                  {this.props.values.status}
                </button>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Credited by:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <p className="font-weight-light ">
                  {this.props.values.createdBy}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Messages:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <p className="font-weight-light">{this.props.values.message}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Client:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <p className="font-weight-light text-success">
                  {this.props.values.client}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Version:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <p className="font-weight-light">{this.props.values.version}</p>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
            <div className="row">
              <div className="col-4  d-flex justify-content-end ">
                <p className="font-weight-bold">Last Updated:</p>
              </div>
              <div className="col-8 d-flex justify-content-start">
                <p>
                  {moment(this.props.values.lastUpdate).format(
                    "DD.MM.YY HH:MM:SS"
                  )}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Created:</p>
              </div>
              <div className="col-8 d-flex justify-content-start align-items-center">
                <p>
                  {moment(this.props.values.created).format(
                    "DD.MM.YY HH:MM:SS"
                  )}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 d-flex justify-content-end align-items-center">
                <p className="font-weight-bold">Participants:</p>
              </div>
              <div className="col-8 m-0">
                <div className="p-0">
                  <Images images={this.props.values.images} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row d-flex">
          <div className="col-xs-12 col-sm-4 col-md-2 col-lg-2 d-sm-flex d-md-flex d-lg-flex justify-content-end">
            <p className="font-weight-bold ">Completed:</p>
          </div>
          <div className="col-xs-12 col-sm-7 col-md-10 col-lg-10">
            <Progress progress={this.props.values.progress} />
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderDetail;
