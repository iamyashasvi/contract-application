import React, { Component } from "react";

class Progress extends Component {
  render() {
    return (
      <>
        <div className="progress mt-1">
          <div
            className="progress-bar progress-bar-striped bg-success"
            role="progressbar"
            style={{ width: this.props.progress }}
            aria-valuenow="25"
            aria-valuemin="0"
            aria-valuemax="100"
          ></div>
        </div>
        <p className="font-weight-light">
          Project completed in{" "}
          <span className="font-weight-bold">{this.props.progress}</span>.
          Remaining close the project, sign a contract and invoice.
        </p>
      </>
    );
  }
}

export default Progress;
