import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      onClick={onClick}
      className={className + " rounded-circle bg-success"}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      onClick={onClick}
      className={className + " rounded-circle bg-success"}
    />
  );
}
export default class ProductImage extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
    };
    return (
      <div className="w-100 h-100">
        <Slider {...settings}>
          {this.props.image.map((element) => {
            return (
              <div className="w-100 h-100" key={element}>
                <img src={element} className="img-fluid" alt={element} />
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
