import React, { Component } from "react";

class Images extends Component {
  render() {
    const images = this.props.images.map((element) => {
      return (
        <div className="col-2 p-0" key={element}>
          <img
            src={require(`../../images/${element}`)}
            className="img-fluid rounded-circle"
            alt={element}
            key={element}
          />
        </div>
      );
    });
    return <div className="col d-flex justify-content-start p-0">{images}</div>;
  }
}

export default Images;
