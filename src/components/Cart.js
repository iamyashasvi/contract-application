import React, { Component } from "react";
import { connect } from "react-redux";
import NumericInput from "react-numeric-input";
import ProductImage from "./ProductImage";
import {  itemChange,addToCart } from "../actions/ProductData";

class Cart extends Component {
  state={
    title:""
  }
  setTitle=(title)=>{
    this.setState({title:title});
  }
  render() {
    let items = this.props.cart.reduce((p, c) => {
      if (c.addToCart) {
        return p + parseInt(c.numberOfItems);
      } else {
        return p;
      }
    }, 0);
    if (isNaN(items)) {
      items = 0;
    }
    if (this.props.product.filter((e) => e.addToCart === true).length === 0) {
      return (
        <div className="d-flex justify-content-center align-items-center mt-5 h-100">
          <div className="alert alert-warning" role="alert">
            <p className="text-muted">
              {" "}
              You should select at least one items to go to cart page
            </p>
          </div>
        </div>
      );
    } else {
      return (
        <>
          <div className="d-flex flex-column m-md-4 m-lg-4 bg-white">
            <div className="container-fluid row mt-3 ">
              <div className="col-6 d-flex align-items-center">
                <h3 className="text-muted">Items in your cart</h3>
              </div>
              <div className="col-6">
                <p className="col-3 offset-7 text-muted">
                  ({items}
                  )Items
                </p>
              </div>
            </div>
            <div>
              <hr />
            </div>
            {this.props.cart.map((item) => {
              let element = this.props.product.filter(e=>e.title===item.title)[0];
                return (
                  <>
                    <div className="container-fluid row mt-3 mb-2" key={element.title}>
                      <div className="col-xs-10 col-sm-12 ml-xs-0 ml-sm-2 pb-sm-2 col-md-2 col-lg-2">
                        <div className="m-md-1 m-lg-1 p-md-0 p-lg-0">
                          <ProductImage image={element.image}/>
                        </div>
                      </div>
                      <div className="ml-lg-3 col-xs-4 col-sm-8 col-md-6 col-lg-7 m-xs-0 d-md-flex d-lg-flex flex-column justify-content-between align-items-start">
                        <div className="row mt-3 mb-3">
                          <div className="col-12">
                            <h2 className="text-success">{element.title}</h2>
                            <p className="text-muted">
                              {element["Product description"][0]}
                            </p>
                          </div>
                        </div>
                        <div className="row mb-3">
                          <div className="col">
                            <h4>Description list</h4>
                            <p className="text-muted">
                              {
                                element.description.filter(
                                  (e) => e.title === "Description lists"
                                )[0]["content"][0]
                              }
                            </p>
                          </div>
                        </div>
                        <div className="row mb-3">
                          <button
                            type="button"
                            className="btn btn-danger ml-3"
                            data-toggle="modal"
                            data-target="#removeItem"
                            onClick={()=>this.setTitle(element.title)}
                          >
                            <i
                              className="fa fa-trash"
                              aria-hidden="true"
                            ></i>{" "}
                            Remove Item
                          </button>
                        </div>
                      </div>
                      <div className="col-xs-3 col-sm-4 p-sm-0 col-md-3 col-lg-2 d-md-flex p-0 d-lg-flex flex-column align-items-center text-muted">
                        <div className="col-12 mt-sm-4 px-0">
                          <div className="d-flex justify-content-around ml-2">
                            <div className="d-flex flex-column align-items-center w-50">
                              <p>
                                {"$"}
                                {element.price}{" "}
                              </p>
                              {element.oldPrice > 0 ? (
                                <p>
                                  <del>
                                    {"$"}
                                    {element.oldPrice}
                                  </del>
                                </p>
                              ) : (
                                ""
                              )}
                              <NumericInput
                                mobile
                                min="0"
                                step="1"
                                value={item.numberOfItems}
                                onChange={(e) =>
                                  this.props.itemChange(element.title, e)
                                }
                                className="form-control"
                              />
                            </div>
                            <div className="text-muted">
                              <h6 className="d-flex justify-content-end">
                                ${item.numberOfItems * element.price}
                              </h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <hr />
                    </div>
                  </>
                );
            })}
            <div className="container-fluid row mt-3 text-muted mb-4">
              <div className="col-6 d-flex align-items-center pl-4">
                <button
                  type="button"
                  onClick={this.props.continueShopping}
                  className="btn btn-white border"
                >
                  <i className="fa fa-arrow-left" aria-hidden="true"></i>{" "}
                  Continue shopping
                </button>
              </div>
              <div className="col-6 d-flex justify-content-end align-items-center pr-0">
                <button
                  type="button"
                  className="btn btn-success border"
                  data-toggle="modal"
                  data-target="#myModal"
                >
                  <i className="fa fa-shopping-cart" aria-hidden="true"></i>{" "}
                  Checkout
                </button>
              </div>
            </div>
          </div>
          <div className="modal text-muted" id="myModal">
            <div className="modal-dialog modal-dialog-scrollable">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Cart summary</h5>
                  <button type="button" className="close" data-dismiss="modal">
                    ×
                  </button>
                  <div>
                    <hr />
                  </div>
                </div>
                <div className="modal-body">
                  <div className="row">
                      <div className="col-4">
                        <h5 className="text-muted">Name</h5>
                      </div>
                      <div className="col-3">
                        <h5 className="text-muted">Quantity</h5>
                      </div>
                      <div className="col-2">
                        <h5 className="text-muted">Price</h5>
                      </div>
                      <div className="col-3">
                        <h5 className="text-muted">Amount</h5>
                      </div>
                    </div>
                  {this.props.cart.map(((e,idx)=>{
                    return(
                      <div className="row" key={idx}>
                        <div className="col-4">
                            {<p>{e.title}</p>}
                        </div>
                        <div className="col-3">
                          {<p>{e.numberOfItems}</p>}
                        </div>
                        <div className="col-2">
                        {<p>{e.price}</p>}
                        </div>
                        <div className="col-3">
                          {<p>{e.price*e.numberOfItems}</p>}
                        </div>
                      </div>
                    )
                  }))}
                  <div>
                    <hr/>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <p>Sum</p>
                        <h2>
                          $
                          {this.props.cart.reduce((p, c,idx) => {
                            return (p = p + c.price * c.numberOfItems);
                          }, 0)}
                      </h2>
                    </div>
                  </div>
                </div>
                <div className="modal-footer d-flex justify-content-between">
                  <button
                    type="button"
                    className="btn btn-success"
                    data-dismiss="modal"
                  >
                    <i
                      className="fa fa-shopping-cart"
                      aria-hidden="true"
                    ></i>{" "}
                      Checkout
                  </button>
                  <button
                    type="button"
                    className="btn btn-warning"
                    data-dismiss="modal"
                  >
                   Cancel
                    </button>
                </div>
              </div>
            </div>
          </div>
          <div className="modal text-muted" id="removeItem">
          <div className="modal-dialog ">
            <div className="modal-content">
                <div className="modal-body">
                  <h4 className="text-muted d-flex justify-content-center">Are you sure want to delete?</h4>
                  <div className="row d-flex justify-content-center">
                  <button
                    type="button"
                    className="btn btn-danger mr-2"
                    data-dismiss="modal"
                    onClick={()=>this.props.addToCart(this.state.title)}
                  >
                    Yes
                  </button>
                  <button
                    type="button"
                    className="btn btn-light ml-2"
                    data-dismiss="modal"
                  >
                   No
                  </button>
                </div>
                </div>
              </div>
              </div>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  product: state.product,
  cart:state.cart
});

export default connect(mapStateToProps, {
  itemChange,addToCart
})(Cart);
