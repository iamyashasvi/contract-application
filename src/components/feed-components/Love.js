import React, { Component } from "react";
import Post from "./Post";

class Love extends Component {
  render() {
    return (
      <p className="font-weight-light mb-1">
        <span className="font-weight-bold">{this.props.from}</span> love{" "}
        <span className="font-weight-bold">{this.props.to}</span> site.
      </p>
    );
  }
}

export default Post(Love);
