import React, { Component } from "react";
import Post from "./Post";

class Photo extends Component {
  render() {
    return (
      <p className="font-weight-light mb-1">
        <span className="font-weight-bold">{this.props.from}</span> add 1 photo
        on <span className="font-weight-bold">{this.props.to}</span>.
      </p>
    );
  }
}

export default Post(Photo);
