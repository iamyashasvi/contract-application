import React, { Component } from "react";
import LikeAndLove from "../LikeAndLove";
import moment from "moment";

const Post = (WrappedComponent) => {
  class Row extends Component {
    render() {
      let a = moment(this.props.time);
      let b = moment();
      let c = moment(this.props.date);
      let image = this.props.image.filter((e) => {
        return e.split(".")[0] === this.props.from;
      });
      return (
        <div key={this.props.from}>
          <div className="row mt-4">
            <div className="col-1 p-0">
              <img
                src={require(`../../../images/${image[0]}`)}
                className="img-fluid rounded-circle"
                alt={image[0]}
              />
            </div>
            <div className="col-9">
              <div className="container">
                <WrappedComponent {...this.props} />
                <p className="text-secondary">
                  {b.diff(c, "days") > 6
                    ? moment(this.props.date).format("DD.MM.YYYY")
                    : moment(this.props.date).calendar() +
                      " - " +
                      moment(this.props.date).format("DD.MM.YYYY")}
                </p>
              </div>
            </div>
            <div className="col-2">
              <p className="text-right">{b.diff(a, "hours") + "h ago"}</p>
            </div>
          </div>
          {this.props.message !== undefined ? (
            <div className="row justify-content-end">
              <div className="col-11">
                <div className="card">
                  <div className="card-body">
                    <p className="card-text">{this.props.message}</p>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
          {this.props.showInteractionButtons ? (
            <div className="row justify-content-end ml-1">
              <div className="col-11">
                <LikeAndLove />
              </div>
            </div>
          ) : (
            ""
          )}
          <hr />
        </div>
      );
    }
  }

  return Row;
};
export default Post;
