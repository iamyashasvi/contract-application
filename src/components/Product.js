import React, { Component } from "react";
import ProductImage from "./ProductImage";
import { connect } from "react-redux";
import { addToCart,itemChange,addUUID } from "../actions/ProductData";
import NumericInput from "react-numeric-input";
class Product extends Component {
  componentDidMount() {
    this.props.addUUID(this.props.product[this.props.index].title);
  }
  componentDidUpdate(prevProp){
    if(this.props.product[this.props.index].title !== prevProp.product[prevProp.index].title){
      this.props.addUUID(this.props.product[this.props.index].title);
    }  
  }
  render() {
    let index = this.props.index;
    return (
      <div className="d-flex flex-column justify-content-end m-sm-4 m-md-4 m-lg-4 m-xs-2 bg-white">
        <div className="row mt-3">
          <div className="col-xl-6 col-md-6 col-sm-12 col-xs-12">
            <div className="row d-flex justify-content-center mt-4 mr-4">
              <div className="col-10">
                <div className="d-flex justify-content-center align-items-center h-100">
                  <ProductImage image={this.props.product[index].image} />
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-6 col-md-6 col-sm-12 col-xs-12 p-sm-4 p-xs-4 d-flex flex-column justify-content-between">
            <div className="d-flex justify-content-start flex-column mt-2 mr-4">
              <div>
                <h1 className="text-muted">
                  {this.props.product[index].title}
                </h1>
                <p className="text-muted">
                  {this.props.product[index].subtitle}
                </p>
                <div className="row d-flex align-items-end mt-4 pt-3 pb-2">
                  <div className="col-auto pb-0 mb-0 mr-0 pr-0">
                    <h1 className="font-weight-light pb-0 mb-0  text-muted">
                      {" $"}
                      {this.props.product[index].price}
                    </h1>
                  </div>
                  <div className="col-auto pb-1">
                    {this.props.product[index].tax > 0 ? (
                      <span className="text-muted">
                        {" "}
                        {this.props.product[index].tax}
                        {"% Tax included"}
                      </span>
                    ) : (
                      <span className="text-muted"> {"Excluded tax"} </span>
                    )}
                  </div>
                </div>
                <hr />
              </div>
              {this.props.product[index]["Product description"] && (
                <div className="mt-4 pb-4">
                  <h3 className="text-muted"> Product description</h3>
                  {this.props.product[index]["Product description"].map((e) => (
                    <p className="text-muted" key={e.split(" ")[0]}>
                      {e}
                    </p>
                  ))}
                </div>
              )}
              {this.props.product[index].description.map((element) => {
                return (
                  <div className="mt-2" key={element.title}>
                    <h4 className="text-muted"> {element.title} </h4>
                    {element.content.map((e) => (
                      <p className="text-muted" key={e.split(" ")[0]}>
                        {e}
                      </p>
                    ))}
                  </div>
                );
              })}
            </div>
            <div>
            <div className="col-md-3 col-lg-3 pl-0">
            <NumericInput
              mobile
              min="0"
              step="1"
              value={this.props.product[index].numberOfItems}
              onChange={(e) =>
              this.props.itemChange(this.props.product[index].title, e)
              }
              className="form-control"
            />
            </div>
              <hr />
              <div className="d-flex justify-content-start">
                <button
                  type="button"
                  onClick={() =>
                    this.props.addToCart(this.props.product[index].title)
                  }
                  className="btn btn-success rounded-left"
                >
                  {this.props.product[index].addToCart ? (
                    <span>
                      <i className="fas fa-cart-plus white"></i>{" "}
                      <span className="white">Added to cart</span>
                    </span>
                  ) : (
                    <span>
                      <i className="fas fa-cart-plus white"></i>{" "}
                      <span className="white">Add to cart</span>
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  product: state.product,
  cart: state.cart,
});

export default connect(mapStateToProps, { addToCart,itemChange,addUUID })(Product);
