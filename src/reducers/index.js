import { combineReducers } from "redux";
import CartReducer from "./CartReducer";
import ProductReducer from "./ProductReducer";
export default combineReducers({ product:ProductReducer,cart: CartReducer});