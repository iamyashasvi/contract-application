import initialState from "../data";
import {ADD_TO_CART,CHANGE_ITEMS} from "../actions/types";
export default function (state = initialState.cart, action) {
  let obj={};
  let arr=[];
  let toInsertItem = false;
  switch (action.type) {
    case ADD_TO_CART:
      action.product.forEach((element)=>{
        if (element.title === action.title) {
          element.addToCart ? toInsertItem=true : toInsertItem=false;
          return Object.assign(obj,{title:element.title},{numberOfItems:element.numberOfItems},{price:element.price});
        }
      });
      if(toInsertItem){
        state.push(obj);
      } else{
        state = state.filter(e=>e.title !== action.title);
      }
      return state;
      case CHANGE_ITEMS:
      arr = state.map((element,i) => {
        if (element!==null) {
          if(element.title === action.title){
          return parseInt(action.text) <= action.product[i].stock || action.text === null
            ? Object.assign(element, { numberOfItems: action.text })
            : Object.assign(element, { numberOfItems: action.product[i].stock });
        }
        return element;
      } else {
          return element;
        }
      });
      return arr;
    default:
      return state;
  }
}
