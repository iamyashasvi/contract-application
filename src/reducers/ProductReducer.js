import { ADD_UUID, ADD_TO_CART, CHANGE_ITEMS } from "../actions/types";
import initialState from "../data";
export default function (state = initialState, action) {
  let arr = [];
  switch (action.type) {
    case ADD_UUID:
      arr = state.map((element) => {
        if (element.title === action.title) {
          return element.hasOwnProperty("uuid") ? element : Object.assign(element,{ uuid: action.uuid },{numberOfItems:1}) ; 
        } else {
          return element;
        }
      });
      return arr;
    case ADD_TO_CART:
      console.log("product reducer");
      arr = state.map((element) => {
        if (element.title === action.title) {
          return element.hasOwnProperty("addToCart") ? Object.assign(element, { addToCart: !element.addToCart }) : Object.assign(element, { addToCart: true });
        } else {
          return element;
        }
      });
      return arr;
    case CHANGE_ITEMS:
      arr = state.map((element,i) => {
        if (element.title === action.title) {
          return parseInt(action.text) <= element.stock || action.text === null
            ? Object.assign(element, { numberOfItems: action.text })
            : Object.assign(element, { numberOfItems: element.stock });
        } else {
          return element;
        }
      });
      return arr;
    default:
      return state;
  }
}
