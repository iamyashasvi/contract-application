import React, { Component } from "react";
import { BrowserRouter, Route, Switch, NavLink,Link } from "react-router-dom";
import HeaderDetail from "./components/HeaderDetail";
import RenderTabs from "./components/RenderTabs";
import Product from "./components/Product";
import Cart from "./components/Cart";
import {connect } from "react-redux";
import "./App.css";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "Active",
      createdBy: "Alex Smith",
      message: 162,
      client: "Zender Company",
      version: "v1.4.2",
      lastUpdate: new Date(2014, 7, 16, 12, 15, 17),
      created: new Date(2014, 6, 10, 23, 36, 57),
      progress: "60%",
      images: [
        "Kim Smith.png",
        "Chris Johnatan Overtunk.jpg",
        "Mark Johnson.png",
        "Monica Smith.png",
        "Kate.jpg",
        "Janet Rosowski.png",
      ],
      one: "active",
      two: "",
      userMessage: [
        {
          type: "message",
          from: "Mark Johnson",
          to: "Monica Smith",
          showInteractionButtons: true,
          message: "A little dog that have knowlege about the human society",
          time: new Date(2020, 5, 15, 9, 10),
          date: new Date(2020, 5, 15, 5, 10),
        },
        {
          type: "message",
          from: "Kim Smith",
          to: "Monica Smith",
          showInteractionButtons: false,
          message:
            "Like all animals lives matter for the earth likewise all religion, casts and color are matter.",
          time: new Date(2020, 5, 15, 5, 10),
          date: new Date(2020, 5, 14, 2, 10),
        },
        {
          type: "photo",
          from: "Janet Rosowski",
          to: "Monica Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 14, 10, 10),
          date: new Date(2020, 5, 13, 10, 10),
        },
        {
          type: "following",
          from: "Chris Johnatan Overtunk",
          to: "Monica Smith",
          showInteractionButtons: true,
          time: new Date(2020, 5, 14, 2, 10),
          date: new Date(2020, 5, 12, 12, 10),
        },
        {
          type: "following",
          from: "Kate",
          to: "Monica Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 13, 19, 10),
          date: new Date(2020, 5, 11, 3, 10),
        },
        {
          type: "love",
          from: "Monica Smith",
          to: "Kim Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 13, 16, 10),
          date: new Date(2020, 5, 11, 5, 10),
        },
      ],
      lastActivity: [
        {
          type: "love",
          from: "Monica Smith",
          to: "Kim Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 13, 14, 10),
          date: new Date(2020, 5, 11, 5, 10),
        },
        {
          type: "following",
          from: "Chris Johnatan Overtunk",
          to: "Monica Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 13, 13, 10),
          date: new Date(2020, 5, 10, 2, 10),
        },
        {
          type: "following",
          from: "Kate",
          to: "Monica Smith",
          showInteractionButtons: true,
          time: new Date(2020, 5, 13, 11, 10),
          date: new Date(2020, 5, 10, 10, 10),
        },
        {
          type: "photo",
          from: "Janet Rosowski",
          to: "Monica Smith",
          showInteractionButtons: false,
          time: new Date(2020, 5, 13, 10, 10),
          date: new Date(2020, 5, 11, 12, 10),
        },
        {
          type: "message",
          from: "Mark Johnson",
          to: "Monika Smith",
          showInteractionButtons: true,
          message: "A little dog that have knowlege about the human society",
          time: new Date(2020, 5, 12, 21, 10),
          date: new Date(2020, 5, 11, 3, 10),
        },
        {
          type: "message",
          from: "Kim Smith",
          to: "Monica Smith",
          showInteractionButtons: false,
          message:
            "Like all animals lives matter for the earth likewise all religion, casts and color are matter.",
          time: new Date(2020, 5, 12, 19, 10),
          date: new Date(2020, 5, 11, 5, 10),
        },
      ],
      navBar1: "active",
      navBar2: "",
      navBar3:"",
      index: 0,
      product1: "active",
      product2: "",
      product3: "",
      product4: "",
      product5: "",
      title:"",
    };
  }

  switchFunction = (buttonType,index)=>{
    switch (buttonType) {
      case "product1":
        return this.setState({
          product1: "active",
          product2: "",
          product3: "",
          product4: "",
          product5: "",
          index: index,
        });
      case "product2":
        return this.setState({
          product1: "",
          product2: "active",
          product3: "",
          product4: "",
          product5: "",
          index: index,
        });
      case "product3":
        return this.setState({
          product1: "",
          product2: "",
          product3: "active",
          product4: "",
          product5: "",
          index: index,
        });
      case "product4":
        return this.setState({
          product1: "",
          product2: "",
          product3: "",
          product4: "active",
          product5: "",
          index: index,
        });
      case "product5":
        return this.setState({
          product1: "",
          product2: "",
          product3: "",
          product4: "",
          product5: "active",
          index: index,
        });
      default:
        return this.setState({
          product1: "active",
          product2: "",
          product3: "",
          product4: "",
          product5: "",
          index: index,
        });
    }
  }


  activeState = (buttonType, index = 0,homePage=false) => {
    if(homePage===true){
      this.switchFunction(buttonType,index);
    }
    return (e) => {
      if (buttonType === "one" || buttonType === "two") {
        buttonType === "one"
          ? this.setState({ one: "active", two: "" })
          : this.setState({ one: "", two: "active" });
      } else {
        this.switchFunction(buttonType,index)
      }
    };
  };

  
  continueShopping = () => {
    return this.setState({
      product1: "active",
      product2: "",
      product3: "",
      product4: "",
      product5: "",
      index: 0,
    });
  };
  toggleNav = (buttonType) => {
      switch (buttonType) {
        case "navBar1":
          this.setState({ navBar1: "active", navBar2: "", navBar3:""});
          break;
        case "navBar2":
          this.setState({ navBar2: "active", navBar1: "", navBar3:""});
          break;
        case "navBar3":
          this.setState({ navBar3: "active", navBar1: "",navBar2:"" });
          break;
        default:
          this.setState({ navBar1: "active", navBar2: "", navBar3:""});
          break;
      }
  };

  toggleOpen = (e) => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  hoverFunc=(title)=>{
    this.setState({title:title})
  }


  render() {
    let nav1 = "nav-item" + this.state.navBar1;
    let nav2 = "nav-item" + this.state.navBar2;
    let nav3 = "nav-item" + this.state.navBar3;
    const classNames = {
      product1: "nav-link " + this.state.product1,
      product2: "nav-link " + this.state.product2,
      product3: "nav-link " + this.state.product3,
      product4: "nav-link " + this.state.product4,
      product5: "nav-link " + this.state.product5,
    };
    return (
      <BrowserRouter>
        <div className="container-fluid bg-light d-flex flex-column">
          <div className="container-fluid bg-light">
            <nav className="navbar navbar-expand-sm bg-light navbar-light">
              <ul className="navbar-nav">
              <li className={nav3}>
                  <NavLink to="/" className="nav-link" onClick={()=>this.toggleNav("navBar1")}>
                    Home
                  </NavLink>
                </li>
                <li className={nav1}>
                  <NavLink to="/project" className="nav-link" onClick={()=>this.toggleNav("navBar2")}>
                    Project
                  </NavLink>
                </li>
                <li className={nav2}>
                  <NavLink to="/product/1" className="nav-link" onClick={()=>this.toggleNav("navBar3")}>
                    Product
                  </NavLink>
                </li>
              </ul>
            </nav>
          </div>
          <div className="container-fluid bg-light">
            <Switch>
              <Route
              exact path="/"
              render={()=>(
                <div className="container d-xs-flex d-sm-flex d-md-flex d-lg-flex flex-xs-column flex-sm-column flex-md-row flex-lg-row bg-light mt-xs-2 mt-sm-2 mt-md-2 mt-lg-3">
                  {this.props.product.map((item,index)=>{
                    let hover = "card w-100 mb-2 m-md-2 m-lg-2"
                    return(
                        <div className={this.state.title === item.title ? hover+" shadow" : hover} key={item.title} onMouseEnter={()=>this.hoverFunc(item.title)} onMouseLeave={()=>this.hoverFunc("")}>
                          <div className="card-img-top card">
                            <img className="img-fluid" src={item.image[0]}  alt={item.title} />
                            <div className="card-img-overlay p-0 m-0 d-flex justify-content-end align-items-end">
                              <h4 className="bg-success text-white m-0 p-xs-0 p-sm-1 p-md-1 p-lg-2">
                                {"$"}{item.price}
                              </h4>
                            </div> 
                          </div>
                          <div className="card-body text-muted d-flex align-items-start flex-column">
                            <h5 className="card-title font-weight-light">{item.category}</h5>
                              <h3>
                                {item.title}
                              </h3>
                              <p className="card-text mt-2">
                                {item["Product description"][0]}
                              </p>
                              <div className="mt-auto">
                                <Link to={`/product/${index+1}`} className="btn border border-success text-success" onClick={()=>this.activeState(`product${index+1}`,index,true)}>info <i className="fa fa-long-arrow-right" aria-hidden="true"></i></Link>
                              </div>
                          </div>
                        </div>
                    )
                  })}
                </div>
              )}
              />
              <Route
                exact
                path="/project"
                render={() => (
                  <div className="bg-white">
                    <HeaderDetail values={this.state} />
                    <RenderTabs
                      values={this.state}
                      activeState={this.activeState}
                    />
                  </div>
                )}
              />
              <Route
                exact
                path={[
                  "/product/1",
                  "/product/2",
                  "/product/3",
                  "/product/4",
                  "/product/5",
                ]}
                render={() => (
                  <>
                  <div className="container">
                      <ul className="nav nav-tabs">
                        <li className="nav-item">
                          <div
                            className={classNames.product1}
                            onClick={this.activeState("product1", 0)}
                          >
                            <NavLink to="/product/1">
                              Desktop publishing software
                            </NavLink>
                          </div>
                        </li>
                        <li className="nav-item">
                          <div
                            className={classNames.product2}
                            onClick={this.activeState("product2", 1)}
                          >
                            <NavLink to="/product/2">Text editor</NavLink>
                          </div>
                        </li>
                        <li className="nav-item">
                          <div
                            className={classNames.product3}
                            onClick={this.activeState("product3", 2)}
                          >
                            <NavLink to="/product/3">CRM software</NavLink>
                          </div>
                        </li>
                        <li className="nav-item">
                          <div
                            className={classNames.product4}
                            onClick={this.activeState("product4", 3)}
                          >
                            <NavLink to="/product/4">Photo editor</NavLink>
                          </div>
                        </li>
                        <li className="nav-item">
                          <div
                            className={classNames.product5}
                            onClick={this.activeState("product5", 4)}
                          >
                            <NavLink to="/product/5">Cart</NavLink>
                          </div>
                        </li>
                      </ul>
                    </div>
                    {this.state.index === 4 ? (
                      <Cart continueShopping={this.continueShopping} />
                    ) : (
                      <Product index={this.state.index} />
                    )}
                  </>
                )}
              />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps =(state)=>({
  product:state.product
});



export default connect(mapStateToProps,null)(App);
